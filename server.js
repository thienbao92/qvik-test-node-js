const express = require("express");
const app = express();
const db = require("./services/db");
const getArticles = require("./services/getArticles");
const bodyParser = require("body-parser");
const updateArticle = require("./services/updateArticle");
const getCategories = (req, res) => {
  res.status(200).send({ categories: db.categories });
};

const getArticleByCategory = (req, res) => {
  const articleId = req.params.categoryId;
  const filteredArticles = getArticles(articleId);

  if (filteredArticles.length !== 0) {
    res.status(200).send({ articles: getArticles(articleId) });
  } else {
    res.status(404).send({
      status: 404,
      message: "Unable to find any articles with this category"
    });
  }
};

const getArticle = (req, res) => {
  const article = db.articles.filter(
    article => article.overview.id === req.params.articleId
  );

  if (!article.length) {
    res.status(404).send({
      message: "cannot find article with ID " + req.params.articleId,
      status: 404
    });
    return;
  }
  res.status(200).send(article[0]);
};

const patchArticle = (req, res) => {
  const articles = db.articles.map(article => article.overview.id);
  const hasArticle = new Set(articles).has(req.params.articleId);

  if (hasArticle) {
    db
      .updateArticle(
        req.params.articleId,
        req.body.starred,
        articles.indexOf(req.params.articleId)
      )
      .then(data => {
        res.status(200).send({
          message: "successfully updated",
          status: 200,
          data: data
        });
      });
    return;
  }

  res.status(404).send({
    message: "Not found",
    status: 404
  });
};

app.use(bodyParser.json());
app.get("/categories", getCategories);

app.get("/categories/:categoryId", getArticleByCategory);
app.get("/articles/:articleId", getArticle);

app.patch("/articles/:articleId", (req, res, next) => {
  if (typeof req.body.starred === "boolean") {
    next();
    return;
  }
  res
    .status(402)
    .send(
      "Unable to get propery 'starred', make sure value is boolean or exist"
    );
});

app.patch("/articles/:articleId", patchArticle);
app.listen(3000, () => console.log("Example app listening on port 3000!"));
