const config = {
  development: {
    port: "3000",
    host: "localhost",
    protocol: "http://"
  },
  production: {
    port: process.env.NODE_ENV,
    host: "localhost",
    protocol: "https://"
  }
};

module.exports = config[process.env.NODE_ENV || "development"];
