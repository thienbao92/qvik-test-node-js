const config = require("../config");
const articlePerCategory = 10;
const rootDb = {
  categories: [
    "Fashion",
    "Science",
    "Auto",
    "Technology",
    "Entertainment",
    "Environment",
    "Finance",
    "Travel"
  ]
};

const generateCategories = () => {
  let categories = [];
  for (let i = 0; i < rootDb.categories.length; i++) {
    const category = rootDb.categories[i];
    categories.push({
      id: category,
      title: category,
      imageUrl:
        config.protocol +
        config.host +
        ":" +
        config.port +
        "/images/" +
        category.toLowerCase() +
        ".png"
    });
  }
  return categories;
};

const assignCategoryId = articles => {
  const assignedArticles = articles.map((article, index) => {
    for (let i = 0; i < rootDb.categories.length; i++) {
      const category = rootDb.categories[i];
      if (
        index >= articlePerCategory * i &&
        index < articlePerCategory * (i + 1)
      ) {
        article.overview.categoryId = category;
      }
    }
    return article;
  });
  return assignedArticles;
};

const generateArticles = () => {
  let articles = [];

  for (let i = 0; i < rootDb.categories.length * articlePerCategory; i++) {
    const articleId = "article_" + i;
    articles.push({
      overview: {
        id: articleId,
        title: "Lorem Ipsum",
        source: "https://www.example.com/articles/" + articleId,
        starred: true,
        timestamp: new Date().getTime()
      },
      body:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque fringilla volutpat erat quis consectetur. Nulla accumsan ipsum eleifend consectetur semper. Nunc in eros augue. Curabitur pretium luctus dui, ac ornare enim malesuada nec. Nullam blandit arcu vitae viverra sagittis. Aliquam hendrerit nibh eget ante volutpat iaculis. Proin malesuada molestie quam eget gravida.",
      imageUrl:
        config.protocol +
        config.host +
        ":" +
        config.port +
        "/images/articles/" +
        articleId +
        ".png"
    });
  }

  return assignCategoryId(articles);
};

const updateArticle = (articleId, status, index) => {
  return new Promise((resolve, reject) => {
    if (db.articles[index].overview.id === articleId) {
      db.articles[index].overview.starred = status;
      resolve(db.articles[index]);
    }
  });
};

const db = {
  categories: generateCategories(),
  articles: generateArticles(),
  updateArticle: updateArticle
};
module.exports = db;
