const db = require("./db");

module.exports = articleId => {
  const articles = db.articles;

  const filteredArticle = articles.filter(
    article => article.overview.categoryId === articleId
  );
  return filteredArticle;
};
